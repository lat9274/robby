##
## Makefile for robby in /home/wu_d/epitech/robby
## 
## Made by mauhoi wu
## Login   <wu_d@epitech.net>
## 
## Started on  Wed Jun 26 00:27:45 2013 mauhoi wu
## Last update Tue Jul  2 18:18:33 2013 mauhoi wu
##

NAME		= robby

SRC		= \
		src/main.c \
		src/telephone.c \
		src/aspirator.c \
		src/d_move.c \
		src/r_move.c \
		src/cuisine.c \
		src/algo_cuisine.c \
		src/my.c

OBJ		= $(SRC:.c=.o)

CC		= gcc

RM		= rm -vf

CFLAGS+=	-Iinclude

all: $(NAME)

$(NAME): $(OBJ)
		$(CC) -o $(NAME) $(OBJ)

clean:
		$(RM) $(OBJ)

fclean: clean
		$(RM) $(NAME)

re: fclean all
