/*
** main.c for robby in /home/wu_d/epitech/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Jun 23 15:41:14 2013 mauhoi wu
** Last update Mon Jul  1 17:04:02 2013 mauhoi wu
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>
#include <telephone.h>
#include <cuisine.h>

int		main(int ac, char **av)
{
  char		sel[1];

  put_present();
  while (1)
    {
      printf(">");
      scanf("%s", &sel);
      if (sel[0] == '1')
	telephone();
      else if (sel[0] == '2')
	aspirator();
      else if (sel[0] == '3')
	while (!cuisine());
      else if (sel[0] == '4')
	{
	  printf("Bye bye !\n");
	  exit(EXIT_SUCCESS);
	}
      else
	printf("Veuillez resaisir votre choix\n");
    }
  return (EXIT_SUCCESS);
}
