/*
** aspirateur.c for robby in /home/wu_d/epitech/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Jun 23 15:40:38 2013 mauhoi wu
** Last update Tue Jul  2 18:12:33 2013 mauhoi wu
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <aspirator.h>

int	open_file(t_map *imap)
{
  FILE	*fp;
  int	n;
  int	i;

  n = 1;
  i = 0;
  if ((fp = fopen("mouv_dab", "r")) != NULL)
    {
      while (n > 0 && !feof(fp) && i < 400)
	{
	  n = fscanf(fp, "%d;%d", &imap->y[i], &imap->x[i]);
	  if (imap->y[i] < 0 || imap->y[i] > 19 ||
	      imap->x[i] < 0 || imap->x[i] > 19)
	    return (1);
	  i++;
	}
      if ((imap->y[i - 1] == 0 || imap->x[i] == 0) || n != 0)
	{
	  if (fclose(fp))
	    return (1);
	  return (0);
	}
    }
  return (1);
}

int	init_map(t_map *imap)
{
  int	y;
  int	x;

  y = 0;
  while (y < 20)
    {
      x = 0;
      while (x < 20)
	{
	  imap->map[y][x] = ' ';
	  x++;
	}
      y++;
    }
  imap->map[0][0] = 'D';
  imap->map[10][10] = 'R';
  return (0);
}

void	print_map(t_map *imap)
{
  int	x;
  int	y;

  y = 0;
  printf("----------------------\n");
  while (y < 20)
    {
      printf("|");
      x = 0;
      while (x < 20)
	{
	  if (imap->map[y][x] < 0)
	    printf(" ");
	  else
	    printf("%c", imap->map[y][x]);
	  x++;
	}
      y++;
      printf("|\n");
    }
  printf("----------------------\n");
  return ;
}

int	aspirator(void)
{
  t_map	imap;
  int	i;

  if (open_file(&imap))
    {
      write(2, "load mouv_dab file fail.\n", 25);
      put_present();
      return (1);
    }
  init_map(&imap);
  print_map(&imap);
  d_move(&imap);
  while (imap.map[0][0] != 'D' || b_place(&imap, 'x') != -1)
    {
      i = r_move(&imap);
      d_move(&imap);
      usleep(500000);
      print_map(&imap);
    }
  printf("Le nombre total de cycle: %d.\n", i);
  return (0);
}
