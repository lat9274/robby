/*
** d_move.c for robby in /home/wu_d/epitech/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Jun 23 17:48:43 2013 mauhoi wu
** Last update Tue Jul  2 18:13:41 2013 mauhoi wu
*/

#include <aspirator.h>

char		d_place(t_map *imap, char c)
{
  char		x;
  char		y;

  y = 0;
  while (y < 20)
    {
      x = 0;
      while (x < 20)
	{
	  if (imap->map[y][x] == 'D')
	    {
	      if (c == 'x')
		return (x);
	      else if (c == 'y')
		return (y);
	    }
	  x++;
	}
      y++;
    }
  return (-1);
}

void		d_move_x(t_map *imap, char m, char x, char y)
{
  if (m == -1 && x > 0)
    {
      imap->map[y][x] = ' ';
      imap->map[y][x - 1] = 'D';
    }
  else if (m == 1 && x < 19)
    {
      imap->map[y][x] = ' ';
      imap->map[y][x + 1] = 'D';
    }
  return ;
}

void		d_move_y(t_map *imap, char m, char x, char y)
{
  if (m == -1 && y > 0)
    {
      imap->map[y][x] = ' ';
      imap->map[y - 1][x] = 'D';
    }
  else if (m == 1 && y < 19)
    {
      imap->map[y][x] = ' ';
      imap->map[y + 1][x] = 'D';
    }
  return ;
}

void		place_ball(t_map *imap, char x, char y)
{
  if (x == 0 && y == 0)
    return ;
  if (x > 0)
    {
      imap->map[y][x] = 'o';
      imap->map[y][x - 1] = 'D';
    }
  else if (x < 19)
    {
      imap->map[y][x] = 'o';
      imap->map[y][x + 1] = 'D';
    }
  return ;
}

int		d_move(t_map *imap)
{
  static int	i = 0;
  char		xi;
  char		yi;

  xi = d_place(imap, 'x');
  yi = d_place(imap, 'y');
  if (imap->x[i] < xi && imap->map[yi][xi - 1] != 'o'
      && imap->map[yi][xi - 1] != 'R')
    d_move_x(imap, -1, xi, yi);
  else if (imap->x[i] > xi && imap->map[yi][xi + 1] != 'o'
	   && imap->map[yi][xi + 1] != 'R')
    d_move_x(imap, 1, xi, yi);
  else if (imap->y[i] < yi && imap->map[yi - 1][xi] != 'o'
	   && imap->map[yi - 1][xi] != 'R')
    d_move_y(imap, -1, xi, yi);
  else if (imap->y[i] > yi && imap->map[yi + 1][xi] != 'o'
	   && imap->map[yi + 1][xi] != 'R')
    d_move_y(imap, 1, xi, yi);
  else if (imap->x[i] == xi && imap->y[i] == yi)
    {
      place_ball(imap, xi, yi);
      if (imap->y[i] != 0 && imap->x[i] != 0)
	i++;
    }
  return (0);
}
