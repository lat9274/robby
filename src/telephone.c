/*
** telephone.c for robby in /home/wu_d/epitech/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Jun 23 15:41:40 2013 mauhoi wu
** Last update Sun Jun 23 15:41:52 2013 mauhoi wu
*/

#include <stdio.h>
#include <my.h>

int	tele_command(char *s, int *a)
{
  if(!strcmp(s, "sonne") && *a == 0)
    {
      printf("Attend\n");
      *a = 1;
    }
  else if(!strcmp(s, "sonne") && *a == 1)
    printf("Vous avez deja sonne.\n");
  else if (!strcmp(s, "allo") && *a == 1)
    printf("Conversation etablie.\nSalut, ici Robby a la pareil.\n");
  else if (!strcmp(s, "allo") && *a != 1)
    printf("Veuillez sonner avant de dire allo.\n");
  else if (!strcmp(s, "bye"))
    {
      printf("Au revoir.\n");
      put_present();
      return (0);
    }
  else
    printf("Je ne comprend pas.\n");
  return (1);
}

int	telephone(void)
{
  char	s[6];
  int	a;

  printf("Dictionnaire :\n<< Sonne, Allo, Bye>>\n");
  a = 0;
  while (1)
    {
      printf(">");
      scanf("%s", s);
      if (!tele_command(s, &a))
	return (0);
    }
  return (1);
}
