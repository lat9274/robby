/*
** r_move.c for robby in /home/wu_d/epitech/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Jun 23 17:48:43 2013 mauhoi wu
** Last update Tue Jul  2 17:50:33 2013 mauhoi wu
*/

#include <aspirator.h>

char		r_place(t_map *imap, char c)
{
  char		x;
  char		y;

  y = 0;
  while (y < 20)
    {
      x = 0;
      while (x < 20)
	{
	  if (imap->map[y][x] == 'R')
	    {
	      if (c == 'x')
		return (x);
	      else if (c == 'y')
		return (y);
	    }
	  x++;
	}
      y++;
    }
  return (-1);
}

char		b_place(t_map *imap, char c)
{
  char		x;
  char		y;

  y = 0;
  while (y < 20)
    {
      x = 0;
      while (x < 20)
	{
	  if (imap->map[y][x] == 'o')
	    {
	      if (c == 'x')
		return (x);
	      else if (c == 'y')
		return (y);
	    }
	  x++;
	}
      y++;
    }
  return (-1);
}

int		r_move_x(t_map *imap, char m, int x, int y)
{
  if (m == -1 && x > 0)
    {
      imap->map[y][x] = ' ';
      imap->map[y][x - 1] = 'R';
    }
  else if (m == 1 && x < 19)
    {
      imap->map[y][x] = ' ';
      imap->map[y][x + 1] = 'R';
    }
  return (0);
}

int		r_move_y(t_map *imap, char m, int x, int y)
{
  if (m == -1 && y > 0)
    {
      imap->map[y][x] = ' ';
      imap->map[y - 1][x] = 'R';
    }
  else if (m == 1 && y < 19)
    {
      imap->map[y][x] = ' ';
      imap->map[y + 1][x] = 'R';
    }
  return (0);
}

int		r_move(t_map *imap)
{
  static int	i = 0;
  int		xi;
  int		yi;

  xi = r_place(imap, 'x');
  yi = r_place(imap, 'y');
  if (i % 2 == 0)
    {
      if (b_place(imap, 'x') == -1)
	return (i);
      else if (b_place(imap, 'x') < xi && imap->map[yi][xi - 1] != 'D')
	r_move_x(imap, -1, xi, yi);
      else if (b_place(imap, 'x') > xi && imap->map[yi][xi + 1] != 'D')
	r_move_x(imap, 1, xi, yi);
      else if (b_place(imap, 'y') < yi && imap->map[yi - 1][xi] != 'D')
	r_move_y(imap, -1, xi, yi);
      else if (b_place(imap, 'y') > yi && imap->map[yi + 1][xi] != 'D')
	r_move_y(imap, 1, xi, yi);
    }
  i++;
  return (i);
}
