/*
** my.c for my in /home/wu_d/epitech/robby
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Tue Jul  2 14:51:53 2013 mauhoi wu
** Last update Tue Jul  2 18:19:19 2013 mauhoi wu
*/

#include <stdio.h>

int	removedash(char *s)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (s[i] && s[i] == '-')
    i++;
  if (s[i] == '\n')
    i++;
  while (s[i])
    {
      s[j] = s[i];
      j++;
      i++;
    }
  s[j] = '\0';
  return (0);
}

int	put_present(void)
{
  printf("Bienvenue dans l'interface de Robby. "
	 "Tapez 1, 2, 3 ou 4 pour acceder au module voulu.\n"
	 "1 - Telephone\n2 - Aspirateur\n3 - Cuisine\n4 - Eteindre Robby\n");
  return (0);
}
