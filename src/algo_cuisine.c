/*
** algo_cuisine.c for algo_cuisine in /home/lat/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Jul  1 16:54:21 2013 mauhoi wu
** Last update Tue Jul  2 17:41:13 2013 mauhoi wu
*/

#include <stdio.h>
#include <string.h>
#include <cuisine.h>

int		check_ingre(t_cui *cuisine, int n)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (cuisine->r_ingre[n][i][0] && cuisine->f_ingre[j][0])
    {
      if (cuisine->r_count[n][i] <= cuisine->f_count[j] &&
	  !strcmp(cuisine->r_ingre[n][i], cuisine->f_ingre[j]))
	{
	  i++;
	  j = -1;
	}
      else if (cuisine->r_count[n][i] > cuisine->f_count[j] &&
	       !strcmp(cuisine->r_ingre[n][i], cuisine->f_ingre[j]))
	return (1);
      j++;
    }
  if (!cuisine->r_ingre[n][i][0])
    return (0);
}

int		list_recette(t_cui *cuisine, int type)
{
  int		n;
  int		re;

  n = 0;
  re = 0;
  while (cuisine->r_name[n][0])
    {
      if (cuisine->r_type[n] == type)
	{
	  if (!check_ingre(cuisine, n))
	    {
	      re++;
	      printf("%d. %s\n", n + 1, cuisine->r_name[n]);
	    }
	}
      n++;
    }
  return (re);
}

int		remove_ingre(t_cui *cuisine, int n)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (cuisine->r_ingre[n][i][0] && cuisine->f_ingre[j][0])
    {
      if (!strcmp(cuisine->r_ingre[n][i], cuisine->f_ingre[j]))
	{
	  cuisine->f_count[j] = cuisine->f_count[j] - cuisine->r_count[n][i];
	  j = -1;
	  i++;
	}
      j++;
    }
  if (!cuisine->r_ingre[n][i][0])
    return (0);
}

int		algo_cuisine(t_cui *cuisine)
{
  int		i;
  char		sel[2];
  static int	type = 1;

  while (type < 4)
    {
      if (list_recette(cuisine, type))
	{
	  printf("Choisir votre plat:");
	  scanf("%s", &sel);
	  if (atoi(sel) && cuisine->r_type[atoi(sel) - 1] == type)
	    {
	      if (!check_ingre(cuisine, atoi(sel) - 1))
		{
		  remove_ingre(cuisine, atoi(sel) - 1);
		  type++;
		}
	    }
	  else
	    printf("Je n'ai pas compris.\n");
	}
      else
	type++;
    }
  return (1);
}
