/*
** cuisine.c for cuisine in /epitech/wu_d/robby/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Thu Jun 27 16:39:34 2013 mauhoi wu
** Last update Tue Jul  2 17:36:38 2013 mauhoi wu
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <cuisine.h>

int		set_r_value(t_cui *cuisine, char *s1, char *s2)
{
  static int	i = 0;
  static int	n = -1;

  if (!isdigit(s2[0]))
    {
      n++;
      strcpy(cuisine->r_name[n], s1);
      cuisine->r_name[n + 1][0] = '\0';
      if (!strcmp(s2, "entree"))
	cuisine->r_type[n] = 1;
      else if (!strcmp(s2, "plat"))
	cuisine->r_type[n] = 2;
      else if (!strcmp(s2, "dessert"))
	cuisine->r_type[n] = 3;
      cuisine->r_type[n + 1] = 0;
      i = 0;
    }
  else
    {
      strcpy(cuisine->r_ingre[n][i], s1);
      cuisine->r_count[n][i] = atoi(s2);
      i++;
      cuisine->r_ingre[n][i][0] = '\0';
    }
  return (0);
}

int		set_f_value(t_cui *cuisine, char *s1, char *s2)
{
  static int	i = 0;

  if (isdigit(s2[0]))
    {
      strcpy(cuisine->f_ingre[i], s1);
      cuisine->f_count[i] = atoi(s2);
      i++;
      cuisine->f_ingre[i][0] = '\0';
    }
  return (0);
}

int		open_recette(t_cui *cuisine)
{
  FILE		*fp;
  char		buff1[100];
  char		buff2[100];
  int		n;

  n = 1;
  if ((fp = fopen("recettes_Robby", "r")) != NULL)
    {
      while (n > 0 && !feof(fp) && n <= 100)
	{
	  n = fscanf(fp, "%[^;];%s\n", &buff1, &buff2);
	  removedash(buff1);
	  set_r_value(cuisine, buff1, buff2);
	}
    }
  else
    return (1);
  if (fclose(fp))
    return (1);
  return (0);
}

int		open_frigo(t_cui *cuisine)
{
  FILE		*fp;
  char		buff1[100];
  char		buff2[100];
  int		n;

  n = 1;
  if ((fp = fopen("frigo_Robby", "r")) != NULL)
    {
      while (n > 0 && !feof(fp) && n <= 100)
	{
	  n = fscanf(fp, "%[^;];%s\n", &buff1, &buff2);
	  set_f_value(cuisine, buff1, buff2);
	}
    }
  else
    return (1);
  if (fclose(fp))
    return (1);
  return (0);
}

int		cuisine(void)
{
  t_cui		cuisine;
  char		sel[1];

  if (open_recette(&cuisine) || open_frigo(&cuisine))
    {
      write(2, "please check recettes_Robby and frigo_Robby files.\n", 51);
      put_present();
      return (1);
    }
  if (algo_cuisine(&cuisine) == 1)
    {
      printf("Repassez la commande ?(Y/N):");
      scanf("%s", &sel);
      if (sel[0] == 'Y')
	return (0);
      else if (sel[0] == 'N')
	{
	  printf("A bientot !\n");
	  put_present();
	  return (2);
	}
    }
  return (1);
}
