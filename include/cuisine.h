/*
** cuisine.h for cuisine in /epitech/wu_d/robby
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Thu Jun 27 16:21:01 2013 mauhoi wu
** Last update Tue Jul  2 17:44:57 2013 mauhoi wu
*/

#ifndef CUISINE_H_
# define CUISINE_H_

typedef struct	s_cui
{
  char		r_name[50][100];
  char		r_ingre[100][30][100];
  int		r_type[50];
  int		r_count[100][30];
  char		f_ingre[100][100];
  int		f_count[100];
}		t_cui;

int		cuisine(void);
int		open_frigo(t_cui *cuisine);
int		open_recette(t_cui *cuisine);
int		set_f_value(t_cui *cuisine, char *s1, char *s2);
int		set_r_value(t_cui *cuisine, char *s1, char *s2);
int		check_ingre(t_cui *cuisine, int n);
int		list_recette(t_cui *cuisine, int type);
int		remove_ingre(t_cui *cuisine, int n);
int		algo_cuisine(t_cui *cuisine);

#endif /* !CUISINE_H_ */
