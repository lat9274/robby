/*
** my.h for robby in /home/wu_d/epitech/robby
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Jun 26 00:27:56 2013 mauhoi wu
** Last update Tue Jul  2 18:19:01 2013 mauhoi wu
*/

#ifndef MY_H_
# define MY_H_

int	removedash(char *s);
int	put_present(void);

#endif /* !MY_H_ */
