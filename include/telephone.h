/*
** telephone.h for robby in /home/wu_d/epitech/robby
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Jun 26 00:28:05 2013 mauhoi wu
** Last update Wed Jun 26 00:28:06 2013 mauhoi wu
*/

#ifndef TELEPHONE_H_
# define TELEPHONE_H_

int	telephone(void);
int	tele_command(char *s, int *a);

#endif /* !TELEPHONE_H_ */
