/*
** aspirator.h for aspirator in /home/wu_d/epitech/robby
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Jun 26 00:29:29 2013 mauhoi wu
** Last update Wed Jun 26 01:19:13 2013 mauhoi wu
*/

#ifndef ASPIRATOR_H_
# define ASPIRATOR_H_

typedef struct	s_map
{
  char		map[20][20];
  char		x[400];
  char		y[400];
}		t_map;

int	open_file(t_map *imap);
int	init_map(t_map *imap);
void	print_map(t_map *imap);
int	aspirator(void);
char	d_place(t_map *imap, char c);
void	d_move_x(t_map *imap, char m, char x, char y);
void	d_move_y(t_map *imap, char m, char x, char y);
void	place_ball(t_map *imap, char x, char y);
int	d_move(t_map *imap);
char	r_place(t_map *imap, char c);
char	b_place(t_map *imap, char c);
int	r_move_y(t_map *imap, char m, int x, int y);
int	r_move(t_map *imap);

#endif /* !ASPIRATOR_H_ */
